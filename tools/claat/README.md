# claat

The claat tool is an open source tool provided by the Google Codelabs team to assist in translating Google Docs into HTML Codelabs.
See the [Google Codelabs Claat](https://github.com/googlecodelabs/tools/blob/master/claat/README.md) Documentation for more details.

## Usage
At Fluid Numerics, we use claat as part of our codelab tutorial development process. There are a number of ways you can use claat

* [Download pre-built claat binaries onto your system](https://github.com/googlecodelabs/tools/releases/tag/v2.2.0)
* Use the Dockerfile in this directory to build a claat container image on your system
* Use the Docker container image for claat hosted at gcr.io/fluid-cluster-dev/claat:latest

### Binary
To use the binary
```
claat export [GDOC-ID]
```
replacing `[GDOC-ID]` with the id of your Google Doc.

### Docker Images
To use the Docker images
```
docker pull gcr.io/fluid-cluster-dev/claat:latest
docker run claat export [GDOC-ID]
```
replacing `[GDOC-ID]` with the id of your Google Doc.
